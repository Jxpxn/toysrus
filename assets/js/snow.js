class Snow {
    constructor(lifetime, speedms) {
        this.events = [];
        this.lifetime = lifetime;
        this.speedms = speedms;
        this.element = null;
    }
    defineEvent(event, func) {
        this.events.push({'event': event, 'func': func});
    }
    emitEvent(event, additional_value) {
        if(this.events.length > 0) {
            this.events.forEach(e => {
                if(e.hasOwnProperty('event') && e.hasOwnProperty('func') && e.event === event) {
                    e.func(additional_value);
                }
            });
        }
    }
    _createElement() {
        this.element = document.createElement('div');
        this.element.style.position = 'absolute';
        this.element.style.top = Math.floor(Math.random() * 100) + ' px';
    }
    _appendElement() {
        document.body.appendChild(this.element);
    }
    _removeElement() {
        document.body.removeChild(this.element);
    }
    _process() {

    }
    run() {
        this._createElement();
        this._appendElement();
        this.savedtime = Date.now();
        this.loop = setInterval(function (e) {
            if(Date.now() - this.savedtime > this.lifetime) {
                this._removeElement();
                clearInterval(this.loop);
            }
            else {
                this.emitEvent('speed', null);
            }
        }, this.speedms);
    }
}

const body = document.body;

let xd = new Snow(3500, 25);
xd.defineEvent('speed', () => {
    console.log('hello');
});
xd.run();
console.log(xd.savedtime);
