<?php

namespace main {
    define('DS', DIRECTORY_SEPARATOR);
    define("ROOT_PATH", dirname(__FILE__) . DS);
    define('APP_PATH', ROOT_PATH . 'app' . DS);
    define('UTILS_PATH', ROOT_PATH . 'utils' . DS);

    /// Config
    require_once APP_PATH . 'config' . DS . 'request_config.php';
    require_once APP_PATH . 'config' . DS . 'mysql_config.php';
    /// Utils
    require_once UTILS_PATH . 'array.php';
    require_once UTILS_PATH . 'string.php';
    require_once UTILS_PATH . 'mysql.php';
    require_once UTILS_PATH . 'math.php';
    /// Main
    require_once APP_PATH . 'app.php';
}

