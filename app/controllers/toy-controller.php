<?php

function toyControllershow(): void {
    /* Récuperer les paramètre de la requête si elle est en POST ou GET */
    $temp = getRequestData();
    /* Transformer le paramètre id en int */
    $flag = !empty($temp['id']);
    if($flag) { $flag = is_numeric($temp['id']) ? MysqlUtils::toyIdExist((int)$temp['id']) : false; };
    if($flag) {
        /* Vérifier si le paramètre store existe et si oui le transformer en int */
        $stock = 0;
        $stock_id = 0;
        $flag = !empty($temp['store']);
        if($flag) { $flag = is_numeric($temp['store']) ? MysqlUtils::toyStoreIdExist((int)$temp['store']) : false; }
        if($flag) {
            $stock_id = $temp['store'];
            $stock = MysqlUtils::getToyStoreStockToyQuantity((int)$temp['store'], (int)$temp['id']);
        }
        else {
            $stock = MysqlUtils::getToyStockQuantity((int)$temp['id']);
        }
        /* Afficher la page*/
        renderPage('toy', ['toy' => MysqlUtils::getToyById((int)$temp['id']), 'stock' => $stock, 'stock_id' => $stock_id]);
    }
    else {
        displayError(404, 'Toy not found :/');
    }
}