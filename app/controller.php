<?php

define('CONTROLLER_PATH', APP_PATH . 'controllers' . DS);

/* Charger un controller */
function loadController(string $controller_name): void {
    if(controllerExist($controller_name)) {
        require_once toControllerName($controller_name);
    }
}

/* Vérifier si un controlleur existe */
function controllerExist(string $controller_name): bool {
    return is_readable(toControllerName($controller_name));
}

/* Transformer un nom de controller en chemin */
function toControllerName(string $controller_name): string {
    return CONTROLLER_PATH . $controller_name . '-controller.php';
}
