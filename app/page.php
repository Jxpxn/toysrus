<?php

define('PAGE_PATH', APP_PATH . 'page' . DS);

/* Afficher une page */
function renderPage(string $page_name, array $additional_data = [], bool $with_header = true, bool $with_footer = true): void {
    $request_data = getRequestData();
    if(pageExist($page_name)) {
        if($with_header) {
            require_once toPagePath('header-site');
        }
        require_once toPagePath($page_name);
        if($with_footer) {
            require_once toPagePath('footer-site');
        }
        mysqli_close(MysqlUtils::$mysql_instance);
    }
    else {
        display404();
    }
}

/* Récupérer les paramètres du lien si il est en GET ou POST sous forme de tableau assoc  */
function getRequestData(): array {
    $output = [];
    if(REQUEST_METHOD === 'GET') {
        $output = $_GET;
    }
    else if(REQUEST_METHOD === 'POST') {
        $output = $_POST;
    }
    return $output;
}

/* Afficher une erreur 404 */
function display404(): void {
    displayError(404, 'Page not found :/');
}

/* Afficher une erreur */
function displayError(int $id, string $message): void {
    renderPage('error', ['id' => $id, 'message' => $message]);
}

/* Vérifier si une page existe */
function pageExist(string $page_name): bool {
    return is_readable(toPagePath($page_name));
}

/* Transormer un nom de page en chemin */
function toPagePath(string $page_name): string {
    return PAGE_PATH . $page_name . '-page.php';
}
