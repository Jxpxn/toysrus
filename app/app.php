<?php

    /// Require manager :3
    require_once 'route.php';
    require_once 'page.php';
    require_once 'controller.php';
    ///Require final
    require_once 'redirector.php';

    MysqlUtils::connectMysql();

    defineRoutes();
    processRedirector();

    /* Définir les routes */
    function defineRoutes() {
        RouteManager::registerRoute(['/', '/main', '/home'], 'main', 'show');
        RouteManager::registerRoute(['/toys'], 'toys', 'show');
        RouteManager::registerRoute(['/toy'], 'toy', 'show');
    }