<?php

    /* Fonction principale du MVC */
    function processRedirector() {
        /* Récuperer les routes */
        $routes = RouteManager::getRoutes();
        $correct_route = null;
        /* Récupérer la route correcte */
        foreach($routes as $route) {
            if(isCorrectRoute($route)) {
                $correct_route = $route;
                break;
            }
        }
        if($correct_route !== null) {
            /* Charger le controller */
            loadController($correct_route->getControllerName());
            /* Vérifier si la fonction du controller de la route existe */
            $temp = $correct_route->getControllerName() . 'Controller' . $route->getAction();
            if(function_exists($temp)) {
                /* Executer la fonction */
                $temp();
            }
            else {
                /* La fonction du controller de la route n'existe pas donc afficher une erreur 404 */
                display404();
            }
        }
        else {
            /* Aucune route ne correspond à l'url actuel donc afficher une erreur 404 */
            display404();
        }
    }

    /* Vérifier si une route est compatible avec l'url actuelle */
    function isCorrectRoute(Route $route): bool {
        $output = false;
        if(!is_null($route)) {
            $temp = false;
            $ways = $route->getWays();
            foreach($ways as $way) {
                if((strEndsWith($way, '*') && strStartsWith(REQUEST_RED_URL, substr($way, 0, strlen($way) - 1))) || REQUEST_RED_URL == $way) {
                    $temp = true;
                    break;
                }
            }
            $output = $temp;
        }
        return $output;
    }