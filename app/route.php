<?php


class Route {
    private $_ways = [];
    private $_page_name;
    private $_action;
    private $_controller_name;
    function __construct(array $ways, string $page_name, string $action, string $controller = 'none') {
        $this->_page_name = $page_name;
        $this->_action = $action;
        $this->_controller_name = $controller == 'none' ? $page_name : $controller;
        $this->defineWays($ways);
    }
    private function defineWays(array $ways): void {
        if(isStringArray($ways)) {
            $this->_ways = $ways;
        }
    }
    public function getWays(): array {
        return $this->_ways;
    }
    public function getPageName(): string {
        return $this->_page_name;
    }
    public function getAction(): string {
        return $this->_action;
    }
    public function getControllerName(): string {
        return $this->_controller_name;
    }
}

class RouteManager {
    private static $route_database = [];

    /* Enregistrer une route dans la liste */
    public static function registerRoute(array $ways, string $page_name, string $action, string $controller = 'none'): void {
        if(isStringArray($ways) && trim($page_name) !== '' && trim($page_name) !== '') {
            $route = new Route($ways, $page_name, $action, $controller);
            array_push(self::$route_database,  $route);
        }
    }

    /* Récupérer les routes enregistrées */
    public static function getRoutes(): array {
        return self::$route_database;
    }
}
