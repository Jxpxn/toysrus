<!doctype html>
<html lang="fr">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>Toysarus</title>
        <script src="https://kit.fontawesome.com/1ce857a6f7.js" crossorigin="anonymous"></script>
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,700&display=swap" rel="stylesheet">
        <link href="../../assets/css/main.css" rel="stylesheet">
    </head>
    <body>
    <a href="/"><img src="../../assets/media/logo.jpg" style="position: absolute; left: 488px; top: 22px;"></a>
    <div class="container">
        <div class="topbar">
            <a href="/toys">Tous les jouets</a>
            <div id="brands">
                <a>Par marque <i class="fas fa-sort-down"></i></a>
                <div>
                    <?php
                    $hey = MysqlUtils::getToyBrands(true);
                    foreach($hey as $row) {
                        echo '<a style="display: block;" href="/toys?brand=' . (string)$row['id'] . '">' . $row['name'] . ' (' . $row['total'] .  ')' . '</a>';
                    }
                    ?>
                </div>
            </div>
            <div id="topbar-extand"></div>
        </div>
