<link href="../../assets/css/error.css" rel="stylesheet">
<div id="error-div">
    <h1>Error <?php if(!empty($additional_data['id'])) { echo (string)$additional_data['id']; } ?>:</h1> <br>
    <h2><?php if(!empty($additional_data['message'])) { echo $additional_data['message']; } ?></h2>
</div>
