<?php
    if(empty($additional_data['toy'])) {
        die();
    }
?>

    <div class="a_toy">
        <div style="display: flex; flex-direction: column; justify-content: center;">
            <div style="display: flex; width: 100%; justify-content: center; flex-wrap: wrap;">
                <label style="text-align: center; font-weight: 700; font-size: 25px; flex-basis: 100%; margin-bottom: 33px;"> <?php echo $additional_data['toy']['name']; ?></label> <br>
            </div>
            <div style="width: 100%; display: flex;">
                <div style="width: 40%; display: flex; flex-direction: column;">
                    <?php
                    echo '<div style="justify-content: center; display: flex;"><img style="width: 220px; height: 220px;" src="../../assets/media/' . $additional_data['toy']['image'] . '"></div>';
                    echo '<h1 style="text-align: center; font-weight: inherit; margin-bottom: 20px; font-size: 48px;">' . str_replace('.', ',', $additional_data['toy']['price']) . ' €</h1>';
                    echo '<form style="display: flex; justify-content: center; margin-bottom: 5px;"><select id="store-selected" name="store">';
                    $general = [];
                    $stores = MysqlUtils::getToyStores();
                    echo '<option value="0">Quel magasin ?</option>';
                    if(!empty($stores)) {
                        $i = 0;
                        foreach($stores as $row) {
                            $i++;
                            $general[$row['id']] = $i;
                            echo '<option value="' . (string)$row['id'] . '">' . $row['name'] . '</option>';
                        }
                    }
                    echo '<input type="hidden" name="id" value="' . $additional_data['toy']['id'] . '">';
                    echo '<input type="submit" value="Ok">';
                    ?>
                    </select></form>
                    <div style="display: flex;">
                    <div style="flex-basis: 105px;">
                    </div>
                    <label style="color: #0056af; font-weight: 700;">Stock:
                        <?php
                            echo '<label style="font-weight: 700; color: black;">' . (string)$additional_data['stock'] . '</label>'
                        ?>
                    </label>
                    </div>
                </div>
                <div style="width: 60%; display: flex; flex-direction: column;">
                    <label style="margin-bottom: 17px;"><label style="color: #0056af;">Marque: </label><label style="font-weight: 700;"><?php echo MysqlUtils::getToyBrandById((int)$additional_data['toy']['brand_id'])['name'] ?></label></label>
                    <?php echo '<div class="toy_desc">' . $additional_data['toy']['description'] . '</div>'; ?>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    let dropbox1 = document.getElementById('store-selected');
    db1_Check();
    function db1_Check() {
        <?php
        if(isset($general[$additional_data['stock_id']])) {
            echo 'dropbox1.options.selectedIndex = ' . (string)$general[$additional_data['stock_id']] . ';';
        }
        ?>
    }
</script>