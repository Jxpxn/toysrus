 <div class="top3">
        <label style="text-align: center; font-weight: bold; font-size: 30px;">Top 3 des Ventes</label>
        <div style="display: flex; justify-content: center;">
            <?php

            $top = MysqlUtils::getTop3Toys();

            for($i = 0; $i < count($top); $i++) {
                $toy = MysqlUtils::getToyById($top[$i]);
                echo '<div style="text-align: center;">';
                echo '<div style="display: flex; justify-content: center; margin-bottom: 13px;"><img style="display:block" src="../../assets/media/' . $toy['image'] . '"></div>';
                echo '<a href="toy?id=' . $toy['id'] . '" style="display: block; height: 48px; margin-bottom: 6px;">' . $toy['name'] . '<a/>';
                echo '<label style="display: block; color: black; font-style: none; font-weight: normal; font-size: 25px">' . str_replace('.', ',', $toy['price']) .' €</label>';
                echo '</div>';
            }

            ?>
        </div>
    </div>
