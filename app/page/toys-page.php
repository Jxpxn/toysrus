<!--<img src="../../assets/media/logo.jpg" style="position: absolute; top: 15px; left: 475px;">-->
    <div class="all-toys">
        <div style="display: flex; flex-direction: column; justify-content: center;">
            <div style="display: flex; width: 100%; justify-content: center; flex-wrap: wrap;">
                <label style="text-align: center; font-weight: bold; font-size: 25px; flex-basis: 100%; margin-bottom: 20px;">Les jouets</label> <br>
                <form>
                    <select id="brand-selected" name="brand">
                        <option value="">Quelle marque ?</option>
                        <?php
                        $general = [];
                        $result = MysqlUtils::getToyBrands();
                        if(!empty($result)) {
                            $i = 0;
                            foreach($result as $row) {
                                $i++;
                                $general[$row['id']] = $i;
                                echo '<option value="' . (string)$row['id'] . '">' . $row['name'] . '</option>';
                            }
                        }
                        ?>
                    </select>
                    <select id="order-selected" name="order">
                        <option value="">Ordre Prix ?</option>
                        <option value="1">Prix Croissant</option>
                        <option value="2">Prix Decroissant</option>
                    </select>
                    <input type="submit" value="Ok">
                </form>
            </div>
        </div>
        <div style="display: flex; justify-content: flex-start; flex-direction: row; flex-wrap: wrap; margin-top: 67px;">
            <?php

            $top = [];

            $filter = !empty($request_data['page']);
            $page_id = $filter ? $request_data['page'] : 1;
            $page_id = is_numeric($page_id) ? $page_id : 1;

            $filter = !empty($request_data['order']);
            $order_id = $filter ? $request_data['order'] : 0;
            $order_id = is_numeric($order_id) ? $order_id : 0;

            if($order_id == 1) {
                $top = MysqlUtils::getToyByOrder(true);
            }
            else if($order_id == 2) {
                $top = MysqlUtils::getToyByOrder(false);
            }
            else {
                $top = MysqlUtils::getToys();
            }

            $filter = !empty($request_data['brand']);
            $filter_id = $filter ? $request_data['brand'] : '0';

            if($filter_id != '0') {
                $temp_list = [];
                foreach($top as $row) {
                    if(!$filter || $row['brand_id'] == $filter_id) {
                        array_push($temp_list, $row);
                    }
                }
                $top = $temp_list;
            }

            $toys_count = count($top);
            $modulo = moduloComplete($toys_count , 4);
            $max_page = $modulo[1] + 1;
            if($page_id > $max_page) {
                $page_id = $max_page;
            }
            $min_toy = ($page_id - 1) * 4;
            $max_toy = $min_toy + 4 - 1;
            $top = getArrayContentByMinAndMax($top, $min_toy, $max_toy);

            foreach($top as $row) {
                $toy = $row;
                echo '<div style="text-align: center; flex-basis: 33.3333%; margin-bottom: 35px;">';
                echo '<div style="display: flex; justify-content: center; margin-bottom: 13px;"><img style="display:block" src="../../assets/media/' . $toy['image'] . '"></div>';
                echo '<a href="toy?id=' . $toy['id'] . '" style="display: block; height: 48px; margin-bottom: 6px;">' . $toy['name'] . '<a/>';
                echo '<label style="display: block; color: black; font-style: none; font-weight: normal; font-size: 25px">' . str_replace('.', ',', $toy['price']) .' €</label>';
                echo '</div>';
            }

            ?>
        </div>
        <div style="display: flex; flex-direction: column; width: 100%; margin-bottom: 20px;">
            <div style="width: 100%; display: flex; justify-content: center;">
                <label style="text-align: center;">Page <?php echo $page_id; ?>/<?php echo $max_page; ?></label>
            </div>
            <div style="display: flex; width: 100%; justify-content: center;">
                <?php
                if($page_id > $max_page) {
                    $page_id = $max_page;
                }
                if($page_id > 1) {
                    echo '<a href="?brand=' . $filter_id . '&order=' . $order_id . '&page=' . ($page_id - 1) . '"><i class="fas fa-arrow-left"></i> Précédent</a>';
                }
                if($page_id < $max_page) {
                    if($page_id > 1) {
                        echo '<div style="padding-left: 20px;"></div>';
                    }
                    echo '<a href="?brand=' . $filter_id . '&order=' . $order_id . '&page=' . ($page_id + 1) . '">Suivant <i class="fas fa-arrow-right"></i></a>';
                }
                ?>
            </div>
        </div>
        <script type="text/javascript">
            let dropbox1 = document.getElementById('order-selected'), dropbox2 = document.getElementById('brand-selected'),
            db1_id = '<?php echo $order_id ?>';

            db1_Check(db1_id);
            db2_Check();

            function db1_Check(value) {
                if(value === '1') {
                    dropbox1.options.selectedIndex = 1;
                }
                else if(value === '2') {
                    dropbox1.options.selectedIndex = 2;
                }
            }

            function db2_Check() {
                <?php
                    if(isset($general[$filter_id])) {
                        echo 'dropbox2.options.selectedIndex = ' . (string)$general[$filter_id] . ';';
                    }
                ?>
            }
        </script>
