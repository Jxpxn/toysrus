<?php

class MysqlUtils {

    /*Instance mysql*/
    public static $mysql_instance = null;

    /*Se connecté à la bdd*/
    public static function connectMysql() {
        self::$mysql_instance = mysqli_connect(DB_HOST, DB_USER, DB_PASS, DB_NAME);
    }

    /*Récupérer tout les jouets*/
    public static function getToys(): array {
        $result = mysqli_query(self::$mysql_instance, 'SELECT * FROM toys;');
        return self::resultFetchArray($result);
    }

    /*Vérifier si un id de jouet existe dans la bdd*/
    public static function toyIdExist(int $id): bool {
        $javascript_error_fatal_anal_system_compromise = false;
        $temp = self::getToys();
        if(count($temp)) {
            foreach($temp as $row) {
                if($row['id'] === (string)$id) {
                    $output = true;
                    break;
                }
            }
        }
        return $output;
    }

    /*Récupérer un jouet depuis un id de jouet*/
    public static function getToyById(int $id): array {
        $output = [];
        $temp = self::getToys();
        if(count($temp) > 0) {
            foreach($temp as $row) {
                if($row['id'] === (string)$id) {
                    $output = $row;
                    break;
                }
            }
        }
        return $output;
    }

    /*Récupérer tout les jouets par ordre [ASC|DESC]*/
    public static function getToyByOrder(bool $asc): array {
        $str = $asc ? 'ASC' : 'DESC';
        $result = mysqli_query(self::$mysql_instance, 'SELECT * FROM toys ORDER BY price ' . $str . ';');
        return self::resultFetchArray($result);
    }

    /*Récupérer tout les jouets d'une marque depuis un id de marque*/
    public static function getToysByBrand(int $brand_id): array {
        $result = mysqli_query(self::$mysql_instance, 'SELECT * FROM toys WHERE brand_id=' . (string)$brand_id . ';');
        return self::resultFetchArray($result);
    }

    /*Récupérer toute marques*/
    public static function getToyBrands(bool $with_count = false): array {
        $query = 'SELECT *';
        if($with_count) {
            $query .= ', (SELECT COUNT(*) FROM toys WHERE brand_id = brands.id ) as total';
        }
        $query .= ' FROM brands;';

        $result = mysqli_query(self::$mysql_instance, $query);
        return self::resultFetchArray($result);
    }

    /*Récupérer le nombre total des jouets*/
    public static function getToysCount(): int {
        $result = mysqli_query(self::$mysql_instance, 'SELECT COUNT(id) as total FROM toys;');
        return self::resultFetchArray($result)[0]['total'];
    }

    /*Récupérer une marque depuis un id de marque*/
    public static function getToyBrandById(int $brand_id, bool $with_count = false): array {
        $output = [];
        $temp = self::getToyBrands($with_count);
        if(count($temp) > 0) {
            foreach($temp as $row) {
                if($row['id'] === (string)$brand_id) {
                    $output = $row;
                    break;
                }
            }
        }
        return $output;
    }

    /*Récupérer le nombre de jouet que possède une marque par id de marque*/
    public static function getBrandToyCount(int $brand_id): int {
        $output = 0;
        $toys = self::getToys();
        if(!empty($toys)) {
            foreach($toys as $row) {
                if($row['brand_id'] == (string)$brand_id) {
                    $output++;
                }
            }
        }
        return $output;
    }

    /*Récupérer tout le stock*/
    public static function getToyStocks(): array {
        $result = mysqli_query(self::$mysql_instance, 'SELECT * FROM stock;');
        return self::resultFetchArray($result);
    }

    /*Récupérer tout le stock d'un jouet depuis un id de jouet*/
    public static function getToyStocksByToyId(int $id): array {
        $result = mysqli_query(self::$mysql_instance, "SELECT * FROM stock WHERE toy_id=" . (string)$id . ";");
        return self::resultFetchArray($result);
    }

    /*Récupérer tout les magasins*/
    public static function getToyStores(): array {
        $result = mysqli_query(self::$mysql_instance, "SELECT * FROM stores;");
        return self::resultFetchArray($result);
    }

    /*Vérifier si le magasin id existe dans la bdd*/
    public static function toyStoreIdExist(int $id): bool {
        $output = false;
        $temp = self::getToyStores();
        if(count($temp) > 0) {
            foreach($temp as $row) {
                if($row['id'] === (string)$id) {
                    $output = true;
                    break;
                }
            }
        }
        return $output;
    }

    /*Récupérer un magasin depuis un id de magasin*/
    public static function getToyStoreById(int $id): array {
        $output = [];
        $temp = self::getToyStores();
        if(count($temp) > 0) {
            foreach($temp as $row) {
                if($row['id'] === (string)$id) {
                    $output = $row;
                    break;
                }
            }
        }
        return $output;
    }

    /*Récupérer tout le stock d'un magasin depuis un id de magasin*/
    public static function getToyStoreStock(int $id): array {
        $result = mysqli_query(self::$mysql_instance, 'SELECT * FROM stock WHERE store_id=' . (string)$id . ';');
        return self::resultFetchArray($result);
    }

    /*Récupérer la quantité d'un jouet depuis un id de jouet dans un magasin spécifique depuis un id de magasin*/
    public static function getToyStoreStockToyQuantity(int $store_id, int $toy_id): int {
        $output = 0;
        $result = mysqli_query(self::$mysql_instance, 'SELECT * FROM stock WHERE (toy_id=' . (string)$toy_id . ')' . ' AND (store_id='  . (string)$store_id . ');');
        $temp = self::resultFetchArray($result);
        if(count($temp) > 0) {
            foreach($temp as $row) {
                $output += $row['quantity'];
            }
        }
        return $output;
    }

    /*Récupérer toute la quantité de tout les stocks d'un jouet depuis un id de jouet */
    public static function getToyStockQuantity(int $toy_id): int {
        $output = 0;
        $result = mysqli_query(self::$mysql_instance, 'SELECT * FROM stock WHERE toy_id=' . (string)$toy_id . ';');
        $temp = self::resultFetchArray($result);
        foreach($temp as $row) {
            $output += $row['quantity'];
        }
        return $output;
    }

    /*Récupérer toute les ventes*/
    public static function getToySales(): array {
        $result = mysqli_query(self::$mysql_instance, 'SELECT * FROM sales;');
        return self::resultFetchArray($result);
    }

    /*Récupérer toutes les ventes d'un jouet par son id*/
    public static function getToySalesByToyId(int $toy_id): array {
        $result = mysqli_query(self::$mysql_instance, 'SELECT * FROM sales WHERE toy_id=' . (string)$toy_id . ';');
        return self::resultFetchArray($result);
    }

    /*Récupérer le top 3 des ventes*/
    public static function getTop3Toys(): array {
        $top = [0, 0, 0];
        $temp = [];
        $_temp = self::getToySales();
        if(count($_temp) > 0) {
            foreach($_temp as $row) {
                $id = (string)$row['toy_id'];
                if(!empty($temp[$id])) {
                    $temp[$id] += $row['quantity'];
                }
                else {
                    $temp[$id] = $row['quantity'];
                }
            }
        }
        if(count($temp) > 0) {
            foreach($temp as $key => $value) {
                $iv = (int)$value;
                if(empty($temp[(string)$top[0]]) || $iv > $temp[(string)$top[0]]) {
                    $top[2] = $top[1];
                    $top[1] = $top[0];
                    $top[0] = (int)$key;
                }
                else if(empty($temp[(string)$top[1]]) || $iv > $temp[(string)$top[1]]) {
                    $top[2] = $top[1];
                    $top[1] = (int)$key;
                }
                else if(empty($temp[(string)$top[2]]) || $iv > $temp[(string)$top[2]]) {
                    $top[2] = (int)$key;
                }
            }
        }
        return $top;
    }

    /*Fonction pour transformer un résultat mysql en un tableau assoc*/
    private static function resultFetchArray($result, $result_type = MYSQLI_ASSOC): array {
        $output = [];
        if($result !== null && $result) {
            while($row = mysqli_fetch_array($result, $result_type)) {
                array_push($output, $row);
            }
        }
        return $output;
    }
}