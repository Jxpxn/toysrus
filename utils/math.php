<?php

/*Modulo Quotien & Reste*/
function moduloComplete(int $a, int $b): array {
    $output = [0, 0];
    if($b > $a || ($a === 0 && $b === 0)) {
        $output = [$a, 0];
    }
    else if($b === $a) {
        $output = [0, 1];
    }
    else if($a < 0 || $b < 0) {
        $output = [0, 0];
    }
    else {
        $q = 0;
        $r = $a;
        while($r >= $b) {
            $q++;
            $r -= $b;
        }
        $output = [$r, $q];
    }
    return $output;
}
