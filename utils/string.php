<?php

/*Vérifier si une string commence par une autre string*/
function strStartsWith(string $a, string $b): bool {
    $output = false;
    $l_a = strlen($a);
    $l_b = strlen($b);
    if($l_a <= 0 || $l_b <= 0 || $l_b > $l_a) {
        return false;
    }
    else if($l_a === $l_b) {
        return $a === $b;
    }
    else {
        return substr($a, 0, $l_b) === $b;
    }
    return $output;
}

/*Vérifier si une string finit par une autre string*/
function strEndsWith(string $a, string $b): bool {
    $output = false;
    $l_a = strlen($a);
    $l_b = strlen($b);
    if($l_a <= 0 || $l_b <= 0 || $l_b > $l_a) {
        return false;
    }
    else if($l_a === $l_b) {
        return $a === $b;
    }
    else {
        return substr($a, $l_a - $l_b) === $b;
    }
    return $output;
}

