<?php

/*Vérifier si l'array contient uniquement des strings*/
function isStringArray(array $arg): bool {
    $output = false;
    if($arg !== null && count($arg) > 0) {
        $output = true;
        for($i = 0; $i < count($arg); $i++) {
            if(!is_string($arg[$i])) {
                $output = false;
                break;
            }
        }
    }
    return $output;
}

/*Récupérer l'item d'un tableau par son index*/
function getArrayItemByIndex(array $array, int $index) {
    $output = null;
    if(!empty($array)) {
        $i = 0;
        foreach($array as $row) {
            if($i === $index) {
                $output = $row;
                break;
            }
            else {
                $i++;
            }
        }
    }
    return $output;
}

/*Récupérer les items d'un tableau depuis une ranger d'index*/
function getArrayContentByMinAndMax(array $array, int $min, int $max): array {
    $output = [];
    if(!empty($array)) {
        if($min >= 0 && $max >= 0 && $min <= $max) {
            for($i = 0; $i < count($array); $i++) {
                if($i >= $min && $i <= $max) {
                    array_push($output, $array[$i]);
                }
            }
        }
    }
    return $output;
}